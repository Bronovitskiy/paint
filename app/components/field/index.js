import SVG from 'svg.js/dist/svg.min';

class Field {
  constructor() {
    this.width = window.innerWidth;
    this.height = 500;
    this.field = SVG('paint').size(this.width, this.height);
  }

  getField() {
    return this.field;
  }
}

let field = new Field();

export function field() {
  return field;
}