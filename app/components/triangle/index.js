import Figure from '../figure/index';

export default class Triangle extends Figure {
  constructor(startX, startY) {
    super();
    this.dimensions = '0,0 100,50 50,100';
    this.color = '#f06';
    this.startX = startX;
    this.startY = startY;
    this.name = this.constructor.name;
  }

  render() {
    const draw = this.getPattern();
    const figure = draw.polygon(this.dimensions);

    super.render(figure, this.color, this.startX, this.startY);
  }
}
