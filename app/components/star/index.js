import Figure from '../figure/index';

export default class Star extends Figure {
  constructor(startX, startY) {
    super();
    this.dimensions = '50,0 60,40 100,50 60,60 50,100 40,60 0,50 40,40';
    this.color = '#f06';
    this.startX = startX;
    this.startY = startY;
    this.name = this.constructor.name;
  }

  render() {
    const draw = this.getPattern();
    const figure = draw.polygon(this.dimensions);

    super.render(figure, this.color, this.startX, this.startY);
  }
}
