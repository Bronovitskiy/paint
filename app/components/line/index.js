import Figure from '../figure/index';

export default class Line extends Figure {
  constructor(startX, startY) {
    super();
    this.dimensions = [0, 100, 100, 0];
    this.color = '#f06';
    this.startX = startX;
    this.startY = startY;
    this.name = this.constructor.name;
  }

  render() {
    const draw = this.getPattern();
    const figure = draw.line(...this.dimensions);
    figure.stroke({ color: this.color, width: 10, linecap: 'round' });

    super.render(figure, this.color, this.startX, this.startY);
  }
}
