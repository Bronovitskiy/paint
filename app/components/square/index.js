import Figure from '../figure';

export default class Square extends Figure {
  constructor(startX, startY) {
    super();
    this.width = 100;
    this.height = 100;
    this.color = '#f06';
    this.startX = startX;
    this.startY = startY;
    this.name = this.constructor.name;
  }

  render() {
    const draw = this.getPattern();
    const figure = draw.rect(this.width, this.height);

    super.render(figure, this.color, this.startX, this.startY);
  }
}
