import Square from '../square';
import Circle from '../circle';
import Ellipse from '../ellipse';
import Line from '../line';
import Star from '../star';
import Triangle from '../triangle';
import FigureList from '../figure-list';

import * as Plugins from '../plugins/';

import * as StoragePlugins from '../storage';


const FIGURE_CONFIG_LIST = {
  ...Plugins,
  Square,
  Circle,
  Ellipse,
  Line,
  Star,
  Triangle,
};


const STORAGE_FUNCTIONS = {
  ...StoragePlugins,
};

const TAGS_NAME = ['polygon', 'circle', 'ellipse', 'line', 'rect']; // TO FIX, USE KEYS FROM FIGURE CONFIG LIST

export default class Paint {
  constructor() {
    this.figureList = new FigureList([]);
    this.figure = null;
    this.init();
    this.renderElement = this.renderElement.bind(this);
    this.clickPaintField = this.clickPaintField.bind(this);
    this.clickMenuItem = this.clickMenuItem.bind(this);
    this.saveState = this.saveState.bind(this);
    this.restoreState = this.restoreState.bind(this);
    this.onStorageMenuClick = this.onStorageMenuClick.bind(this);
    this.addListeners();
  }

  init() {
    this.renderFiguresMenu();
    this.renderStorageMenu();
  }

  renderFiguresMenu() {
    const menu = document.querySelector('.menu');

    Object.keys(FIGURE_CONFIG_LIST).forEach((key) => {
      const div = document.createElement('div');
      div.className = "menu__item";
      const title = key.replace(/([A-Z])/g, " $1").toLowerCase();
      div.innerHTML = title;
      div.dataset.figure = key;
      menu.appendChild(div);
    })
  }

  renderStorageMenu() {
    const menu = document.querySelector('.footer');

    Object.keys(STORAGE_FUNCTIONS).forEach((key) => {
      const safeDiv = document.createElement('div');
      safeDiv.className = 'function__item';
      safeDiv.setAttribute('style', 'cursor: pointer')
      safeDiv.innerHTML = `Save to ${key.toUpperCase()}`;
      safeDiv.dataset.fnName = key;
      safeDiv.dataset.action = 'save';
      menu.appendChild(safeDiv);

      const readDiv = document.createElement('div');
      readDiv.className = 'function__item';
      readDiv.setAttribute('style', 'cursor: pointer;margin-top: 5px;margin-bottom: 20px;')
      readDiv.innerHTML = `Read from ${key.toUpperCase()}`;
      readDiv.dataset.fnName = key;
      readDiv.dataset.action = 'restore';
      menu.appendChild(readDiv);
    })
  }

  start() {
  }

  addListeners() {
    document.querySelectorAll('.menu__item').forEach((element) => {
      element.addEventListener('click', this.clickMenuItem);
    });

    document.querySelectorAll('.function__item').forEach((element) => {
      element.addEventListener('click', this.onStorageMenuClick);
    });
    document.querySelector('#paint').addEventListener('click', this.clickPaintField);
  }

  renderElement(figureName, x, y) {
    const figure = FIGURE_CONFIG_LIST[figureName];
    const params = { x, y };
    const object = figure.deserialization(params);

    this.figureList.add(object);

    object.render();
  }

  clickPaintField(event) {
    if (this.figure && event.target.tagName === "svg") {
      const currentX = event.clientX - 50;
      const currentY = event.clientY - 50;
      this.renderElement(this.figure, currentX, currentY);
    } else if (TAGS_NAME.includes(event.target.tagName)){

      const deleteFigure = confirm("Do you want to delete the figure?");
      const id = event.target.id;

      if (deleteFigure) {
        const newfigureList = this.figureList.getFigures().filter((element) => {

          if (element.svgElementId === id) {
            element.svgElement.remove();
            return false;
          }

          return true;
        });

        this.figureList.figures = newfigureList;
      } else {

        const inputPosition = confirm("Do you want to change coordinates");

        if (inputPosition) {
          const currentX = prompt("X", '');
          const currentY = prompt("Y", '');

          if(currentX && currentY) {
            this.figureList.getFigures().forEach((element) => {
              if (element.svgElementId === id) {
                element.move(+currentX, +currentY)
              }
            });
          }

        }
      }
    }
  }

  clickMenuItem(event) {
    const figureName = event.target.dataset['figure'];

    const inputPosition = confirm("Do you want to enter coordinates?");

    if (inputPosition) {
      const currentX = prompt("X", '');
      const currentY = prompt("Y", '');
      if(currentX && currentY) {
        this.renderElement(figureName, +currentX, +currentY);
      }
    } else {
      this.figure = figureName;
    }
  }

  onStorageMenuClick(event) {
    const { fnName, action } = event.target.dataset;

    if (action === 'save') {
      this.saveState(fnName)
    } else if(action === 'restore') {
      this.restoreState(fnName)
    }
  }

  saveState(fnName) {
    const jsFiguresParams = this.figureList.getFigures().map(f => f.serialize());
    const serializedData = STORAGE_FUNCTIONS[fnName].serialize(jsFiguresParams);
    localStorage.setItem('paint', serializedData);
  }

  restoreState(fnName) {
    const serializedData = localStorage.getItem('paint');
    if (serializedData) {
      this.clearPaintArea();
      const jsFiguresParams = STORAGE_FUNCTIONS[fnName].deserialize(serializedData);
      jsFiguresParams.forEach(item => this.renderElement(item.name, item.startX, item.startY))
    }
  }

  clearPaintArea() {
    this.figureList.removeAll();
    const svg = document.querySelector('#paint svg');
    while (svg.firstChild) {
      svg.removeChild(svg.firstChild);
    }
  }
}
