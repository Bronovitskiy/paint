export default class FigureList {
  constructor(figures) {
    this.figures = figures;
  }

  add(figure) {
    this.figures.push(figure)
  }

  getFigures() {
    return this.figures;
  }

  removeAll() {
    this.figures = [];
  }
}