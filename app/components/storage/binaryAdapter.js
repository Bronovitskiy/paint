import BinaryGen from '../lib/binaryGen';
import SerializerBase from '../lib/serializerBase';

const binaryGen = new BinaryGen();

export default class BinaryAdapter extends SerializerBase {
  static serialize(jsFiguresParams) {
    return binaryGen.generateBin(jsFiguresParams);
  }

  static deserialize(sarializedData) {
    return binaryGen.parseBin(sarializedData);
  }
}
