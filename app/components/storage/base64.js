import SerializerBase from '../lib/serializerBase';

export default class Base64Serializer extends SerializerBase {
  static serialize(jsFiguresParams) {
    return btoa(JSON.stringify(jsFiguresParams));
  }

  static deserialize(jsonString) {
    return JSON.parse(atob(jsonString));
  }
}
