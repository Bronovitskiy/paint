import SerializerBase from '../lib/serializerBase';

export default class JSONSerializer extends SerializerBase {
  static serialize(jsFiguresParams) {
    return JSON.stringify(jsFiguresParams);
  }

  static deserialize(jsonString) {
    return JSON.parse(jsonString);
  }
}
