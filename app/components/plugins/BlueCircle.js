import Figure from '../figure';

export default class BlueCircle extends Figure {
  constructor(startX, startY) {
    super();
    this.width = 200;
    this.height = 200;
    this.color = '#0000FF';
    this.startX = startX;
    this.startY = startY;
    this.name = this.constructor.name;
  }

  render() {
    const draw = this.getPattern();
    const figure = draw.circle(this.width);

    super.render(figure, this.color, this.startX, this.startY);
  }
}
