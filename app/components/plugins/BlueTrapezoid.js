import Figure from '../figure';

export default class BlueTrapezoid extends Figure {
  constructor(startX, startY) {
    super();
    this.points = [[0, 0], [0, 50], [100, 70], [100, 0]];
    this.color = '#0000FF';
    this.startX = startX;
    this.startY = startY;
    this.name = this.constructor.name;
  }

  render() {
    const draw = this.getPattern();
    const figure = draw.polyline(this.points);

    super.render(figure, this.color, this.startX, this.startY);
  }
}
