export default class BinaryGen {
  generateBin(data) {
    return new TextEncoder('utf-8').encode(JSON.stringify(data)).toString();
  }

  parseBin(data) {
    const decoder = new TextDecoder("utf-8");
    const array = new Uint8Array(data.split(',').map(e => parseInt(e)));
    return JSON.parse(decoder.decode(array));
  }
}
