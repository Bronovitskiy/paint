import { field } from '../field';

export default class Figure {
  constructor() {
    this.svgElement = null;
    this.svgElementId = null;
  }

  getPattern() {
    return field().getField();
  }

  render(object, color, startX, startY) {
    object.fill(color).move(startX, startY);
    this.svgElement = object;
    this.svgElementId = object.id();
  }

  move(startX, startY) {
    this.startX = startX;
    this.startY = startY;
    this.svgElement.move(startX, startY);
  }

  serialize() {
    let figureParams = {
      name: this.name,
      startX: this.startX,
      startY: this.startY,
    };

    return figureParams;
  }

  static deserialization(params) {
    return new this(params.x, params.y);
  }
}
